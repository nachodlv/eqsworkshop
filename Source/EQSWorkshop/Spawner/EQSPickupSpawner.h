﻿#pragma once

#include "CoreMinimal.h"
#include "EnvironmentQuery/EnvQueryTypes.h"
#include "UObject/Object.h"

#include "EQSPickupSpawner.generated.h"

class AEQSPickup;

UCLASS(Blueprintable, BlueprintType)
class EQSWORKSHOP_API UEQSPickupSpawner : public UObject
{
	GENERATED_BODY()

public:
	void StartSpawning();

	UFUNCTION(BlueprintPure)
	const TArray<AEQSPickup*>& GetPickups() const { return Pickups; }
	
protected:
	virtual void BeginDestroy() override;
	
	void Spawn();
	
	void AddNewPickup(AEQSPickup* NewPickup);

	void OnSpawnEQSFinished(TSharedPtr<FEnvQueryResult> Result);

	UFUNCTION()
	void OnPickupDestroyed(AActor* DestroyedActor);
	
private:
	UPROPERTY(EditAnywhere, Category = "EQS")
	float TimeToSpawn = 10.0f;

	UPROPERTY(EditAnywhere, Category = "EQS")
	int32 QuantityOfSpawns = 5;

	UPROPERTY(EditAnywhere, Category = "EQS")
	FEQSParametrizedQueryExecutionRequest EQS;

	UPROPERTY(EditAnywhere, Category = "EQS")
	TSubclassOf<AEQSPickup> PickupClass;

	FTimerHandle SpawnLoopHandle;

	UPROPERTY(Transient)
	TArray<TObjectPtr<AEQSPickup>> Pickups;
};
