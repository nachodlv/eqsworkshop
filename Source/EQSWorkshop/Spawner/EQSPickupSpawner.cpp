﻿#include "EQSPickupSpawner.h"

#include "EngineUtils.h"
#include "EQSPickup.h"

void UEQSPickupSpawner::StartSpawning()
{
	for (auto It = TActorIterator<AEQSPickup>(GetWorld()); It; ++It)
	{
		AddNewPickup(*It);
	}
	Spawn();
}

void UEQSPickupSpawner::Spawn()
{
	FQueryFinishedSignature Delegate = FQueryFinishedSignature::CreateUObject(this, &UEQSPickupSpawner::OnSpawnEQSFinished);
	EQS.Execute(*this, nullptr, Delegate);
}

void UEQSPickupSpawner::AddNewPickup(AEQSPickup* NewPickup)
{
	Pickups.Add(NewPickup);
	NewPickup->OnDestroyed.AddDynamic(this, &UEQSPickupSpawner::OnPickupDestroyed);
}

void UEQSPickupSpawner::OnSpawnEQSFinished(TSharedPtr<FEnvQueryResult> Result)
{
	if (Result->IsSuccessful())
	{
		UWorld* World = GetWorld();
		for (int32 i = 0; i < Result->Items.Num() && i < QuantityOfSpawns; ++i)
		{
			const FVector ItemLocation = Result->GetItemAsLocation(i);
			FActorSpawnParameters Params;
			Params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			AEQSPickup* NewPickup = World->SpawnActor<AEQSPickup>(PickupClass, ItemLocation, FRotator::ZeroRotator, Params);
			AddNewPickup(NewPickup);
		}
	}

	FTimerManager& TimerManager = GetWorld()->GetTimerManager();
	const FTimerDelegate Delegate = FTimerDelegate::CreateUObject(this, &UEQSPickupSpawner::Spawn);
	TimerManager.SetTimer(SpawnLoopHandle, Delegate, TimeToSpawn, false);
}

void UEQSPickupSpawner::OnPickupDestroyed(AActor* DestroyedActor)
{
	AEQSPickup* Pickup = Cast<AEQSPickup>(DestroyedActor);
	Pickup->OnDestroyed.RemoveDynamic(this, &UEQSPickupSpawner::OnPickupDestroyed);
	Pickups.Remove(Pickup);
}

void UEQSPickupSpawner::BeginDestroy()
{
	for (AEQSPickup* Pickup : Pickups)
	{
		Pickup->OnDestroyed.RemoveDynamic(this, &UEQSPickupSpawner::OnPickupDestroyed);
	}
	Pickups.Reset();
	
	Super::BeginDestroy();
}
