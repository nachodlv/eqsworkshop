﻿#pragma once

#include "CoreMinimal.h"
#include "DataProviders/AIDataProvider.h"
#include "GameplayTagContainer.h"

#include "AIDataProvider_EQSCharacterTag.generated.h"

USTRUCT()
struct FEQSValueByCharacterTag
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, meta = (Categories = "Character.AI"))
	FGameplayTag CharacterTag;

	UPROPERTY(EditAnywhere)
	float Value;
};

UCLASS()
class EQSWORKSHOP_API UAIDataProvider_EQSCharacterTag : public UAIDataProvider
{
	GENERATED_BODY()

public:
	virtual void BindData(const UObject& Owner, int32 RequestId) override;
	
private:
	UPROPERTY()
	float Value;

	UPROPERTY(EditAnywhere, Category = "EQS")
	TArray<FEQSValueByCharacterTag> ValuesByCharacterTag;
};
