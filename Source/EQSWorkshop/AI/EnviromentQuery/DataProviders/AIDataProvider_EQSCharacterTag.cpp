﻿#include "AIDataProvider_EQSCharacterTag.h"

#include "EQSWorkshop/AI/EQSWAICharacter.h"

void UAIDataProvider_EQSCharacterTag::BindData(const UObject& Owner, int32 RequestId)
{
	if (const AEQSWAICharacter* Character = Cast<AEQSWAICharacter>(&Owner))
	{
		for (const FEQSValueByCharacterTag& ValueByCharacterTag : ValuesByCharacterTag)
		{
			if (Character->GetCharacterTag().MatchesTag(ValueByCharacterTag.CharacterTag))
			{
				Value = ValueByCharacterTag.Value;
				break;
			}
		} 
	}
	else
	{
		if (ValuesByCharacterTag.Num() > 0)
		{
			Value = ValuesByCharacterTag[0].Value;
		}
	}
}
