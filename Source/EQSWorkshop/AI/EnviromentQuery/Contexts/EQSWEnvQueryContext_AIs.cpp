﻿#include "EQSWEnvQueryContext_AIs.h"

#include "EngineUtils.h"
#include "EnvironmentQuery/EnvQueryTypes.h"
#include "EnvironmentQuery/Items/EnvQueryItemType_Actor.h"
#include "EQSWorkshop/AI/EQSWAICharacter.h"

void UEQSWEnvQueryContext_AIs::ProvideContext(FEnvQueryInstance& QueryInstance, FEnvQueryContextData& ContextData) const
{
	TArray<AActor*> Actors;
	for (auto It = TActorIterator<AEQSWAICharacter>(QueryInstance.World); It; ++It)
	{
		Actors.Add(*It);
	}
	UEnvQueryItemType_Actor::SetContextHelper(ContextData, Actors);
}
