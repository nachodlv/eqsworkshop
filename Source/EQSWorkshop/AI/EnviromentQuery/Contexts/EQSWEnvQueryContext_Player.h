﻿#pragma once

// UE Includes
#include "CoreMinimal.h"
#include "EnvironmentQuery/EnvQueryGenerator.h"

#include "EQSWEnvQueryContext_Player.generated.h"

/** Provides the query with the player */
UCLASS()
class EQSWORKSHOP_API UEQSWEnvQueryContext_Player : public UEnvQueryContext
{
	GENERATED_BODY()

public:
	virtual void ProvideContext(FEnvQueryInstance& QueryInstance, FEnvQueryContextData& ContextData) const override;
};
