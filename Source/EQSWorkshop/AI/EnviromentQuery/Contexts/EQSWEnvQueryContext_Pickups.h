﻿#pragma once

#include "CoreMinimal.h"
#include "EnvironmentQuery/EnvQueryContext.h"

#include "EQSWEnvQueryContext_Pickups.generated.h"

/** Provides the query with the pickups currently present in the world */
UCLASS()
class EQSWORKSHOP_API UEQSWEnvQueryContext_Pickups : public UEnvQueryContext
{
	GENERATED_BODY()

public:
	virtual void ProvideContext(FEnvQueryInstance& QueryInstance, FEnvQueryContextData& ContextData) const override;
};
