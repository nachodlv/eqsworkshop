﻿#include "EQSEnvQueryTest_PickupInUse.h"

#include "EnvironmentQuery/Items/EnvQueryItemType_Actor.h"
#include "EQSWorkshop/Spawner/EQSPickup.h"

UEQSEnvQueryTest_PickupInUse::UEQSEnvQueryTest_PickupInUse()
{
	ValidItemType = UEnvQueryItemType_Actor::StaticClass();
	Cost = EEnvTestCost::Low;
	SetWorkOnFloatValues(false);
}

void UEQSEnvQueryTest_PickupInUse::RunTest(FEnvQueryInstance& QueryInstance) const
{
	BoolValue.BindData(QueryInstance.Owner.Get(), QueryInstance.QueryID);
	const bool bShouldBeInUse = BoolValue.GetValue();

	for (FEnvQueryInstance::ItemIterator It(this, QueryInstance); It; ++It)
	{
		const AEQSPickup* Pickup = CastChecked<AEQSPickup>(QueryInstance.GetItemAsActor(It.GetIndex()));
		It.SetScore(TestPurpose, FilterType, Pickup->GetActorUsingPickup() != nullptr, bShouldBeInUse);
	}
}
