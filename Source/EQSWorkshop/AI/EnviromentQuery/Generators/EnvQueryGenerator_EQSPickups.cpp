﻿#include "EnvQueryGenerator_EQSPickups.h"
#include "EngineUtils.h"
#include "EnvironmentQuery/Items/EnvQueryItemType_Actor.h"
#include "EQSWorkshop/EQSWorkshopGameMode.h"
#include "EQSWorkshop/Spawner/EQSPickup.h"
#include "EQSWorkshop/Spawner/EQSPickupSpawner.h"

UEnvQueryGenerator_EQSPickups::UEnvQueryGenerator_EQSPickups()
{
	ItemType = UEnvQueryItemType_Actor::StaticClass();
}

void UEnvQueryGenerator_EQSPickups::GenerateItems(FEnvQueryInstance& QueryInstance) const
{
	if (const AEQSWorkshopGameMode* GameMode = QueryInstance.World->GetAuthGameMode<AEQSWorkshopGameMode>())
	{
		const UEQSPickupSpawner* PickupSpawner = GameMode->GetPickupSpawner();
		for (AEQSPickup* Pickup : PickupSpawner->GetPickups())
		{
			QueryInstance.AddItemData<UEnvQueryItemType_Actor>(Pickup);
		}
	}
	else
	{
		// For testing pawn
		for (auto It = TActorIterator<AEQSPickup>(QueryInstance.World); It; ++It)
		{
			QueryInstance.AddItemData<UEnvQueryItemType_Actor>(*It);
		}
	}
}
