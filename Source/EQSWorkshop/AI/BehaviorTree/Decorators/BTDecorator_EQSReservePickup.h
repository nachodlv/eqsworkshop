﻿#pragma once

#include "CoreMinimal.h"

#include "BehaviorTree/BTDecorator.h"

#include "EQSWorkshop/AI/BehaviorTree/Tasks/BTTask_EQSClearBBKey.h"
#include "EQSWorkshop/Spawner/EQSPickup.h"

#include "UObject/Object.h"
#include "BTDecorator_EQSReservePickup.generated.h"

UCLASS()
class EQSWORKSHOP_API UBTDecorator_EQSReservePickup : public UBTDecorator
{
	GENERATED_BODY()

public:
	UBTDecorator_EQSReservePickup();
	AEQSPickup* GetPickupFromBlackboard(UBehaviorTreeComponent& OwnerComp) const;

	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;

	virtual void OnBecomeRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	virtual void OnCeaseRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	
private:
	UPROPERTY(EditAnywhere, Category = "EQS")
	FBlackboardKeySelector PickupKey;

	UPROPERTY(EditAnywhere, Category = "EQS")
	bool bReservePickupWhileRelevant = false;
};
