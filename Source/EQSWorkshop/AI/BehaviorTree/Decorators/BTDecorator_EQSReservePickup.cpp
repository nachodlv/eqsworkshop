﻿
#include "BTDecorator_EQSReservePickup.h"

#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "EQSWorkshop/Spawner/EQSPickup.h"

UBTDecorator_EQSReservePickup::UBTDecorator_EQSReservePickup()
{
	PickupKey.AddObjectFilter(this, GET_MEMBER_NAME_CHECKED(UBTDecorator_EQSReservePickup, PickupKey), AEQSPickup::StaticClass());
	bNotifyBecomeRelevant = true;
	bNotifyCeaseRelevant = true;
}

bool UBTDecorator_EQSReservePickup::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp,
	uint8* NodeMemory) const
{
	const APawn* Pawn = OwnerComp.GetAIOwner()->GetPawn();
	const AEQSPickup* Pickup = GetPickupFromBlackboard(OwnerComp);
	return !Pickup->GetActorUsingPickup() || Pickup->GetActorUsingPickup() == Pawn;
}

void UBTDecorator_EQSReservePickup::OnBecomeRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AEQSPickup* Pickup = GetPickupFromBlackboard(OwnerComp);
	Pickup->SetActorUsingPickup(OwnerComp.GetAIOwner()->GetPawn());
}

void UBTDecorator_EQSReservePickup::OnCeaseRelevant(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	if (AEQSPickup* Pickup = GetPickupFromBlackboard(OwnerComp))
	{
		Pickup->SetActorUsingPickup(nullptr);
	}
}

AEQSPickup* UBTDecorator_EQSReservePickup::GetPickupFromBlackboard(UBehaviorTreeComponent& OwnerComp) const
{
	const UBlackboardComponent* BBComp = OwnerComp.GetBlackboardComponent();
	return Cast<AEQSPickup>(BBComp->GetValueAsObject(PickupKey.SelectedKeyName));
}
