﻿#include "BTTask_EQSUsePickup.h"

#include "AIController.h"
#include "EngineUtils.h"

#include "BehaviorTree/BlackboardComponent.h"

#include "EQSWorkshop/Spawner/EQSPickup.h"

void FEQSUsePickupMemory::Reset()
{
	UsePickupHandle.Invalidate();
	PickupInUse.Reset();
}

UBTTask_EQSUsePickup::UBTTask_EQSUsePickup()
{
	bNotifyTick = true;

	PickupKey.AddObjectFilter(this, GET_MEMBER_NAME_CHECKED(UBTTask_EQSUsePickup, PickupKey), AEQSPickup::StaticClass());
}

EBTNodeResult::Type UBTTask_EQSUsePickup::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	FEQSUsePickupMemory* Memory = CastInstanceNodeMemory<FEQSUsePickupMemory>(NodeMemory);
	Memory->Reset();
	
	AEQSPickup* Pickup = Cast<AEQSPickup>(OwnerComp.GetBlackboardComponent()->GetValueAsObject(PickupKey.SelectedKeyName));

	if (!Pickup)
	{
		return EBTNodeResult::Failed;
	}

	Pickup->SetActorUsingPickup(OwnerComp.GetAIOwner()->GetPawn());
	Memory->PickupInUse = Pickup;

	FTimerManager& TimerManager = OwnerComp.GetWorld()->GetTimerManager();
	const FTimerDelegate Delegate = FTimerDelegate::CreateUObject(this, &UBTTask_EQSUsePickup::PickupFinishedUsing, &OwnerComp);
	TimerManager.SetTimer(Memory->UsePickupHandle, Delegate, Pickup->GetUseTime(), false);

	return EBTNodeResult::InProgress;
}

EBTNodeResult::Type UBTTask_EQSUsePickup::AbortTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	FEQSUsePickupMemory* Memory = CastInstanceNodeMemory<FEQSUsePickupMemory>(NodeMemory);
	if (Memory->PickupInUse.IsValid())
	{
		Memory->PickupInUse->SetActorUsingPickup(nullptr);
	}
	FTimerManager& TimerManager = OwnerComp.GetWorld()->GetTimerManager();
	TimerManager.ClearTimer(Memory->UsePickupHandle);
	return EBTNodeResult::Aborted;
}

void UBTTask_EQSUsePickup::TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	APawn* Pawn = OwnerComp.GetAIOwner()->GetPawn();
	const float TextHeight = Pawn->BaseEyeHeight + 50.0f;
	const FVector TextLocation = Pawn->GetActorLocation() + FVector::UpVector * TextHeight;
	DrawDebugString(OwnerComp.GetWorld(), TextLocation, FString("Using pickup"), nullptr, FColor::White, 0.0f, true);
}

void UBTTask_EQSUsePickup::PickupFinishedUsing(UBehaviorTreeComponent* OwnerComp)
{
	const int32 InstanceIdx = OwnerComp->FindInstanceContainingNode(this);
	const FEQSUsePickupMemory* Memory = CastInstanceNodeMemory<FEQSUsePickupMemory>(OwnerComp->GetNodeMemory(this, InstanceIdx));

	if (Memory->PickupInUse.IsValid())
	{
		OwnerComp->GetWorld()->DestroyActor(Memory->PickupInUse.Get());
	}

	FinishLatentTask(*OwnerComp, EBTNodeResult::Succeeded);
}

