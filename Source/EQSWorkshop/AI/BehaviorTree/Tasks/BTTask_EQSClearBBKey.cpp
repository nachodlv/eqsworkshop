﻿#include "BTTask_EQSClearBBKey.h"

#include "BehaviorTree/BlackboardComponent.h"

EBTNodeResult::Type UBTTask_EQSClearBBKey::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	UBlackboardComponent* BBComp = OwnerComp.GetBlackboardComponent();
	BBComp->ClearValue(BBToClear.SelectedKeyName);
	return EBTNodeResult::Succeeded;
}
