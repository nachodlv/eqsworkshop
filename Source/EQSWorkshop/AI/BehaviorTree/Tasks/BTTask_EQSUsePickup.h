﻿#pragma once

#include "CoreMinimal.h"

#include "BehaviorTree/BTTaskNode.h"

#include "UObject/Object.h"
#include "BTTask_EQSUsePickup.generated.h"

class AEQSPickup;

struct FEQSUsePickupMemory
{
	FTimerHandle UsePickupHandle;
	TWeakObjectPtr<AEQSPickup> PickupInUse = nullptr;

	void Reset();
};

UCLASS()
class EQSWORKSHOP_API UBTTask_EQSUsePickup : public UBTTaskNode
{
	GENERATED_BODY()

public:
	UBTTask_EQSUsePickup();
	
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	virtual EBTNodeResult::Type AbortTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	virtual void TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;

	virtual uint16 GetInstanceMemorySize() const override { return sizeof(FEQSUsePickupMemory); }

protected:
	void PickupFinishedUsing(UBehaviorTreeComponent* OwnerComp);
	
private:
	UPROPERTY(EditAnywhere, Category = "EQS")
	FBlackboardKeySelector PickupKey;
	
};
