﻿#pragma once

// UE Includes
#include "CoreMinimal.h"
#include "GameplayTagContainer.h"

#include "GameFramework/Character.h"

#include "EQSWAICharacter.generated.h"

UCLASS()
class EQSWORKSHOP_API AEQSWAICharacter : public ACharacter
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintPure)
	const FGameplayTag& GetCharacterTag() const { return CharacterTag; }
	
private:
	UPROPERTY(EditAnywhere, Category = "EQS", meta = (Categories = "Character.AI"))
	FGameplayTag CharacterTag;
};
