// Copyright Epic Games, Inc. All Rights Reserved.

#include "EQSWorkshopGameMode.h"
#include "EQSWorkshopCharacter.h"
#include "Spawner/EQSPickupSpawner.h"
#include "UObject/ConstructorHelpers.h"

AEQSWorkshopGameMode::AEQSWorkshopGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}

void AEQSWorkshopGameMode::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);

	PickupSpawner = NewObject<UEQSPickupSpawner>(this, PickupSpawnerClass);
	PickupSpawner->StartSpawning();
}
