// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"

#include "EQSWorkshopGameMode.generated.h"

class UEQSPickupSpawner;

UCLASS(minimalapi)
class AEQSWorkshopGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AEQSWorkshopGameMode();

	virtual void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage) override;

	UFUNCTION(BlueprintPure)
	UEQSPickupSpawner* GetPickupSpawner() const { return PickupSpawner; }

private:
	UPROPERTY(EditAnywhere, Category = "EQS")
	TSubclassOf<UEQSPickupSpawner> PickupSpawnerClass;

	UPROPERTY(Transient)
	TObjectPtr<UEQSPickupSpawner> PickupSpawner;
};



